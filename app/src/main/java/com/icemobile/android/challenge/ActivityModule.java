package com.icemobile.android.challenge;

import com.icemobile.android.challenge.api.NewsRetriever;
import com.icemobile.android.challenge.ui.ActivityTitleController;
import com.icemobile.android.challenge.ui.DetailFragment;
import com.icemobile.android.challenge.ui.HomeActivity;
import com.icemobile.android.challenge.ui.HomeFragment;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This module represents objects which exist only for the scope of a single activity. We can safely create singletons using the activity instance
 * because the entire object graph will only ever exist inside of that activity.
 */
@Module(
        injects = {
                HomeActivity.class,
                HomeFragment.class,
                DetailFragment.class
        },
        addsTo = AndroidModule.class,
        library = true
)
public class ActivityModule {

    private final BaseActivity activity;

    public ActivityModule(BaseActivity activity) {
        this.activity = activity;
    }

    /**
     * Allow the activity context to be injected but require that it be annotated with {@link ForActivity @ForActivity} to explicitly differentiate it
     * from application context.
     */
    @Provides
    @Singleton
    @ForActivity
    Context provideActivityContext() {
        return activity;
    }

    @Provides
    @Singleton
    ActivityTitleController provideTitleController() {
        return new ActivityTitleController(activity);
    }

    @Provides
    @Singleton
    BaseActivity provideActivity() {
        return activity;
    }

    @Provides
    @Singleton
    NewsRetriever provideNewsRetriever() {
        return new NewsRetriever();
    }

    @Provides
    @Singleton
    FragmentManager provideFragmentManager() {
        return activity.getSupportFragmentManager();
    }
}
