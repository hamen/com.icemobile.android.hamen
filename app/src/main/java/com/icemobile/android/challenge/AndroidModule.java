package com.icemobile.android.challenge;

import com.alterego.advancedandroidlogger.implementations.DetailedAndroidLogger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * A module for Android-specific dependencies which require a {@link Context} or {@link android.app.Application} to create.
 */
@Module(library = true)
public class AndroidModule {

    private final BaseApplication application;

    public AndroidModule(BaseApplication application) {
        this.application = application;
    }

    /**
     * Allow the application context to be injected but require that it be annotated with {@link ForApplication @ForApplication} to explicitly
     * differentiate it from an activity context.
     */
    @Provides
    @Singleton
    @ForApplication
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    DetailedAndroidLogger provideLogger() {
        return application.getLogger();
    }
}
