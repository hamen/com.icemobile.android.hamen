package com.icemobile.android.challenge;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends ActionBarActivity {

    private ObjectGraph activityGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Create the activity graph by .plus-ing our modules onto the application graph.
        BaseApplication application = (BaseApplication) getApplication();
        activityGraph = application.getApplicationGraph().plus(getModules().toArray());

        super.onCreate(savedInstanceState);

        activityGraph.inject(this);

        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

    }

    @Override
    protected void onDestroy() {
        activityGraph = null;
        super.onDestroy();
    }


    protected List<Object> getModules() {
        return Arrays.<Object>asList(new ActivityModule(this));
    }

    public void inject(Object object) {
        activityGraph.inject(object);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(new CalligraphyContextWrapper(newBase));
    }
}
