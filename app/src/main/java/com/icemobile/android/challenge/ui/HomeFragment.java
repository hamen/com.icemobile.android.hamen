package com.icemobile.android.challenge.ui;

import com.alterego.advancedandroidlogger.implementations.DetailedAndroidLogger;
import com.icemobile.android.challenge.BaseActivity;
import com.icemobile.android.challenge.BaseFragment;
import com.icemobile.android.challenge.R;
import com.icemobile.android.challenge.URLS;
import com.icemobile.android.challenge.adapters.NewsAdapter;
import com.icemobile.android.challenge.api.NewsRetriever;
import com.icemobile.android.challenge.api.models.News;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Observer;

public class HomeFragment extends BaseFragment {

    private NewsAdapter mAdapter;

    private List<News> mNews = new ArrayList<News>();

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Inject
    ActivityTitleController mTitleController;

    @Inject
    BaseActivity mActivity;

    @Inject
    NewsRetriever mNewsRetriever;

    @Inject
    DetailedAndroidLogger mLogger;

    @Inject
    FragmentManager mFragmentManager;

    @InjectView(R.id.listView)
    GridView mGridView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_newslist, container, false);
        ButterKnife.inject(this, rootView);
        setHasOptionsMenu(true);

        mAdapter = new NewsAdapter(inflater, mNews);
        mGridView.setAdapter(mAdapter);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DetailFragment detailFragment = DetailFragment.newInstance(mNews.get(position));
                mFragmentManager.beginTransaction()
                        .replace(android.R.id.content, detailFragment)
                        .addToBackStack("detail")
                        .commit();
            }
        });

        refreshNewsList();

        return rootView;
    }

    private void refreshNewsList() {
        mActivity.setSupportProgressBarIndeterminateVisibility(true);

        mNewsRetriever
                .getNews(URLS.NEWS)
                .subscribe(new Observer<List<News>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        mLogger.error(throwable.toString());
                    }

                    @Override
                    public void onNext(List<News> newses) {
                        mNews = newses;
                        mAdapter.setNews(newses);
                        mActivity.setSupportProgressBarIndeterminateVisibility(false);
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();

        // Fragments should not modify things outside of their own view. Use an external controller to
        // ask the activity to change its title.
        mTitleController.setTitle(mActivity.getString(R.string.news_page_title));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).inject(this);
    }
}
