package com.icemobile.android.challenge;

public class URLS {

    public static final String NEWS = "http://appel.icemobile.com/newsreader/news.json";

    public static final String DETAIL_PAGE = "http://appel.icemobile.com/newsreader/detail/{ID}.json";

    public static final String IMAGE = "http://appel.icemobile.com/newsreader/thumbs/";

}
