package com.icemobile.android.challenge.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.icemobile.android.challenge.api.models.News;
import com.icemobile.android.challenge.api.models.NewsDetail;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

@Singleton
public class NewsRetriever {

    private final OkHttpClient mOkHttpClient = new OkHttpClient();

    public Observable<List<News>> getNews(String url) {
        return getNewsJson(url)
                .flatMap(new Func1<String, Observable<List<News>>>() {
                    @Override
                    public Observable<List<News>> call(String json) {
                        return getNewsList(json);
                    }
                });
    }

    private Observable<String> getNewsJson(final String url) {
        return Observable
                .create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        Request request = new Request.Builder()
                                .url(url)
                                .build();

                        Response response = null;
                        try {
                            response = mOkHttpClient.newCall(request).execute();

                            if (!response.isSuccessful()) {
                                throw new IOException("Unexpected code " + response);
                            }
                            subscriber.onNext(response.body().string());

                        } catch (IOException e) {
                            subscriber.onError(e);
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<List<News>> getNewsList(final String json) {
        return Observable
                .create(new Observable.OnSubscribe<List<News>>() {
                    @Override
                    public void call(Subscriber<? super List<News>> subscriber) {

                        Type listType = new TypeToken<List<News>>() {
                        }.getType();
                        List<News> list = new Gson().fromJson(json, listType);

                        subscriber.onNext(list);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<NewsDetail> getNewsDetail(String url) {
        return getNewsJson(url)
                .map(new Func1<String, NewsDetail>() {
                    @Override
                    public NewsDetail call(String json) {
                        return new Gson().fromJson(json, NewsDetail.class);
                    }
                });
    }
}
