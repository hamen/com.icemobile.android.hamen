package com.icemobile.android.challenge.ui;

import com.alterego.advancedandroidlogger.implementations.DetailedAndroidLogger;
import com.icemobile.android.challenge.BaseActivity;
import com.icemobile.android.challenge.BaseFragment;
import com.icemobile.android.challenge.R;
import com.icemobile.android.challenge.URLS;
import com.icemobile.android.challenge.adapters.NewsAdapter;
import com.icemobile.android.challenge.api.NewsRetriever;
import com.icemobile.android.challenge.api.models.News;
import com.icemobile.android.challenge.api.models.NewsDetail;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DetailFragment extends BaseFragment {

    @InjectView(R.id.headline)
    TextView mHeadline;

    @InjectView(R.id.tagline)
    TextView mTagline;

    @InjectView(R.id.author)
    TextView mAuthor;

    @InjectView(R.id.date)
    TextView mDate;

    @InjectView(R.id.body)
    TextView mBody;

    @InjectView(R.id.thumb)
    ImageView mThumb;

    @InjectView(R.id.detail_box)
    RelativeLayout mDetailBox;

    private NewsAdapter mAdapter;

    private News mNews;

    private String mID;

    public static DetailFragment newInstance() {
        return new DetailFragment();
    }

    @Inject
    ActivityTitleController mTitleController;

    @Inject
    BaseActivity mActivity;

    @Inject
    NewsRetriever mNewsRetriever;

    @Inject
    DetailedAndroidLogger mLogger;

    public static DetailFragment newInstance(News news) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("NEWS", news);

        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(bundle);
        return detailFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_newsdetail, container, false);
        ButterKnife.inject(this, rootView);
        setHasOptionsMenu(true);

        Bundle bundle = getArguments();
        mNews = ((News) bundle.getParcelable("NEWS"));

        refreshNewsList();

        return rootView;
    }

    private void refreshNewsList() {
        mActivity.setSupportProgressBarIndeterminateVisibility(true);

        mNewsRetriever
                .getNewsDetail(URLS.DETAIL_PAGE.replace("{ID}", String.valueOf(mNews.getId())))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NewsDetail>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(NewsDetail newsDetail) {
                        mHeadline.setText(newsDetail.getHeadline());
                        mAuthor.setText(newsDetail.getAuthor());

                        DateTimeFormatter dateStringFormat = DateTimeFormat.forPattern("yyyyMMdd HHmm");
                        DateTime dateTime = dateStringFormat.parseDateTime(newsDetail.getDate() + " " + newsDetail.getTime());

                        mDate.setText(dateTime.toLocalDate().toString() + " " + dateTime.getHourOfDay() + ":" + dateTime.getMinuteOfHour());

                        mTagline.setText(newsDetail.getTagline());
                        mBody.setText(newsDetail.getBodytext());

                        String thumbUrl = mNews.getThumbnail().replace(".jpg", "").concat("@2x.png");
                        ImageLoader.getInstance().displayImage(URLS.IMAGE + thumbUrl, mThumb);

                        mActivity.setSupportProgressBarIndeterminateVisibility(false);
                        mDetailBox.setVisibility(View.VISIBLE);
                    }
                });

    }

    @Override
    public void onResume() {
        super.onResume();

        // Fragments should not modify things outside of their own view. Use an external controller to
        // ask the activity to change its title.
        mTitleController.setTitle(mActivity.getString(R.string.news_page_title));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).inject(this);
    }
}
