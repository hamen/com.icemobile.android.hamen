package com.icemobile.android.challenge.api.models;

import com.google.gson.annotations.Expose;

import lombok.Data;

@Data
public class NewsDetail {

    @Expose
    private Integer id;

    @Expose
    private String headline;

    @Expose
    private String tagline;

    @Expose
    private String author;

    @Expose
    private String date;

    @Expose
    private String time;

    @Expose
    private String bodytext;
}