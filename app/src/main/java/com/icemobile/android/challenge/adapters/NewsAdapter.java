package com.icemobile.android.challenge.adapters;

import com.icemobile.android.challenge.R;
import com.icemobile.android.challenge.URLS;
import com.icemobile.android.challenge.api.models.News;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class NewsAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;

    private List<News> mNews;

    public NewsAdapter(LayoutInflater inflater, List<News> newsList) {
        mInflater = inflater;
        mNews = newsList;
    }

    @Override
    public int getCount() {
        return mNews.size();
    }

    @Override
    public News getItem(int position) {
        return mNews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mNews.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.news_list_item, parent, false);

            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        News news = getItem(position);

        holder.author.setText(news.getAuthor());

        DateTimeFormatter dateStringFormat = DateTimeFormat.forPattern("yyyyMMdd HHmm");
        DateTime dateTime = dateStringFormat.parseDateTime(news.getDate() + " " + news.getTime());

        holder.date.setText(dateTime.toLocalDate().toString() + " " + dateTime.getHourOfDay() + ":" + dateTime.getMinuteOfHour());

        holder.headline.setText(news.getHeadline());

        String thumbUrl = news.getThumbnail().replace(".jpg", "").concat("@2x.png");
        ImageLoader.getInstance().displayImage(URLS.IMAGE + thumbUrl, holder.thumb);

        return convertView;
    }

    public void setNews(List<News> news) {
        mNews.clear();
        mNews.addAll(news);
        notifyDataSetChanged();
    }

    static class ViewHolder {

        @InjectView(R.id.thumb)
        ImageView thumb;

        @InjectView(R.id.headline)
        TextView headline;

        @InjectView(R.id.author)
        TextView author;

        @InjectView(R.id.date)
        TextView date;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
