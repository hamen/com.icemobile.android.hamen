package com.icemobile.android.challenge.api.models;

import com.google.gson.annotations.Expose;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Data;

@Data
public class News implements Parcelable {

    @Expose
    private Integer id;

    @Expose
    private String headline;

    @Expose
    private String author;

    @Expose
    private String date;

    @Expose
    private String time;

    @Expose
    private String thumbnail;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.headline);
        dest.writeString(this.author);
        dest.writeString(this.date);
        dest.writeString(this.time);
        dest.writeString(this.thumbnail);
    }

    private News(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.headline = in.readString();
        this.author = in.readString();
        this.date = in.readString();
        this.time = in.readString();
        this.thumbnail = in.readString();
    }

    public static final Parcelable.Creator<News> CREATOR = new Parcelable.Creator<News>() {
        public News createFromParcel(Parcel source) {
            return new News(source);
        }

        public News[] newArray(int size) {
            return new News[size];
        }
    };
}