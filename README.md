The app has been tested on Motorola E (Android 4.4.2) and Nexus10 (Android 4.4.4).

* It's Gradle based and GIT + GIT FLOW versioned
* I used fragments, of course
* Dagger as dependency injection system
* ButterKnife as View Injector
* Lombok to save a bit of Java Boilerplate
* Gson and JodaTime to handle the API JSON response
* RxJava as Reactive Programming framework
* OkHttp as network library
* Univeral Image Loader as image downloading/caching system
* Advanved Android Logger as logging system